## Qwertee API Server

Get the qwertee shirts https://www.qwertee.com and expose them into a RESTful API



## Qwertee Client

Retrieve data from qwertee-api-server and use them in a sample WPF app



## Relevent files:

- QwerteeApiServer/QwerteeApiServer/Controllers/ShirtsController.cs

- QwerteeApiServer/QwerteeApiServer/Controllers/TimeLeftController.cs

- QwerteeApiServer/QwerteeApiServer/Models/QwerteeSource.cs

- QwerteeApiServer/QwerteeApiServer/Models/ShirtModel.cs

- QwerteeApiServer/QwerteeApiServer/Models/ShirtPrice.cs

- QwerteeApiServer/QwerteeApiServer/Models/TimeLeft.cs

- QwerteeApiClient/