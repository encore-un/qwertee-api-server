﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using RestSharp;
using Newtonsoft.Json;

namespace QwerteeClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        readonly DispatcherTimer _timer = new DispatcherTimer();
        readonly DateTime _endDate;

        // shirts
        public List<ShirtModel> QwerteeShirts = QwerteeService.GetShirts();

        // shirts' images
        public string Shirt0ImageLink => QwerteeShirts[0].ImageLink.Replace("//", "https://");
        public string Shirt1ImageLink => QwerteeShirts[1].ImageLink.Replace("//", "https://");
        public string Shirt2ImageLink => QwerteeShirts[2].ImageLink.Replace("//", "https://");
        public string Shirt3ImageLink => QwerteeShirts[3].ImageLink.Replace("//", "https://");
        public string Shirt4ImageLink => QwerteeShirts[4].ImageLink.Replace("//", "https://");
        public string Shirt5ImageLink => QwerteeShirts[5].ImageLink.Replace("//", "https://");


        public MainWindow()
        {
            InitializeComponent();
            
            // shirts' name
            Shirt0NameLabel.Content = QwerteeShirts[0].Name;
            Shirt1NameLabel.Content = QwerteeShirts[1].Name;
            Shirt2NameLabel.Content = QwerteeShirts[2].Name;
            Shirt3NameLabel.Content = QwerteeShirts[3].Name;
            Shirt4NameLabel.Content = QwerteeShirts[4].Name;
            Shirt5NameLabel.Content = QwerteeShirts[5].Name;

            // shirts' prices
            Shirt0TeePrice.Content = QwerteeShirts[0].TeePrice.ToString();
            Shirt1TeePrice.Content = QwerteeShirts[1].TeePrice.ToString();
            Shirt2TeePrice.Content = QwerteeShirts[2].TeePrice.ToString();
            Shirt3TeePrice.Content = QwerteeShirts[3].TeePrice.ToString();
            Shirt4TeePrice.Content = QwerteeShirts[4].TeePrice.ToString();
            Shirt5TeePrice.Content = QwerteeShirts[5].TeePrice.ToString();

        // time left
        var qwerteeTimeLeft = QwerteeService.GetTimeLeft();

            var timeToGo = new TimeSpan(qwerteeTimeLeft.Hours, qwerteeTimeLeft.Minutes, qwerteeTimeLeft.Seconds);

            _timer.Tick += TimerTick;
            _timer.Interval = new TimeSpan(0, 0, 1);
            _endDate = DateTime.Now.Add(timeToGo);

            _timer.Start();
        }

        void TimerTick(object sender, EventArgs e)
        {
            TimerLabel.Content = ToStringTimeSpan(_endDate - DateTime.Now);

            if (_endDate == DateTime.Now)
            {
                _timer.Stop();
            }
        }

        private object ToStringTimeSpan(TimeSpan time)
        {
            return $"{time.Hours} hours {time.Minutes} minutes {time.Seconds} seconds left";
        }
    }

    public static class QwerteeService
    {
        public static RestClient Client = new RestClient("http://localhost:22007/api");

        public static TimeLeft GetTimeLeft()
        {
            var request = new RestRequest("timeleft", Method.GET);
            return JsonConvert.DeserializeObject<TimeLeft>(Client.Execute(request).Content);
        }

        public static List<ShirtModel> GetShirts()
        {
            var request = new RestRequest("shirts", Method.GET);
            return JsonConvert.DeserializeObject<List<ShirtModel>>(Client.Execute(request).Content);
        }
    }

    public class TimeLeft
    {
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public int Seconds { get; set; }

        public override string ToString()
        {
            return $"{Hours} hours {Minutes} minutes {Seconds} seconds left";
        }
    }

    public class ShirtPrice
    {
        public decimal Eur { get; set; }
        public decimal Gbp { get; set; }
        public decimal Usd { get; set; }

        public override string ToString()
        {
            return $"€{Eur} / £{Gbp} / ${Usd}";
        }
    }

    public class ShirtModel
    {
        public string Name { get; set; }
        public string ImageLink { get; set; }
        public ShirtPrice TeePrice { get; set; }
        public ShirtPrice SweaterPrice { get; set; }
        public ShirtPrice HoodiePrice { get; set; }
        public ShirtPrice PrintPrice { get; set; }
    }
}
