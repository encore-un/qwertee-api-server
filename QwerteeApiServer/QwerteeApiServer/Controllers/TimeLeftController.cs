﻿using System;
using System.Web.Http;
using QwerteeApiServer.Models;

namespace QwerteeApiServer.Controllers
{
    /// <summary>
    /// TimeLeft controller
    /// </summary>
    public class TimeLeftController : ApiController
    {
        private readonly TimeLeft _time = new TimeLeft();

        /// <summary>
        /// Default TimeLeftController constructor
        /// </summary>
        public TimeLeftController()
        {
            var endTime = DateTime.Today.AddHours(23);
            var now = DateTime.Now;

            if (now.Hour == 23)
            {
                endTime = endTime.AddDays(1);
            }

            var timeLeft = endTime - now;
            _time.Hours = timeLeft.Hours;
            _time.Minutes = timeLeft.Minutes;
            _time.Seconds = timeLeft.Seconds;
        }

        // GET api/timeleft
        /// <summary>
        /// Retrieve time left for today's sale
        /// </summary>
        /// <returns>A time left object</returns>
        [Route("api/timeleft")]
        public TimeLeft GetAllTimeLeft()
        {
            return _time;
        }
    }
}
