﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using QwerteeApiServer.Models;

namespace QwerteeApiServer.Controllers
{
    /// <summary>
    /// Shirts controller
    /// </summary>
    public class ShirtsController : ApiController
    {
        private readonly List<ShirtModel> _todaysTees = new List<ShirtModel>();

        /// <summary>
        /// Default ShirtsController constructor
        /// </summary>
        public ShirtsController()
        {
            var qwerteeDocument = QwerteeSource.GetDocument();

            // div nodes containing tee name and prices
            var collection = qwerteeDocument.DocumentNode.Descendants("div").Where(d =>
                d.Attributes.Contains("class") &&
                d.Attributes["class"].Value.Contains("index-tee") &&
                d.Attributes.Contains("data-name")
            );

            foreach (var node in collection)
            {
                // img node containing tee image
                var teeImage = node.ChildNodes.Descendants("img").Where(d => 
                    d.Attributes.Contains("src")
                );

                var tee = new ShirtModel
                {
                    Name = node.Attributes["data-name"].Value,
                    TeePrice = new ShirtPrice
                    {
                        Eur = Convert.ToDecimal(node.Attributes["data-tee-price-eur"].Value),
                        Gbp = Convert.ToDecimal(node.Attributes["data-tee-price-gbp"].Value),
                        Usd = Convert.ToDecimal(node.Attributes["data-tee-price-usd"].Value)
                    },
                    SweaterPrice = new ShirtPrice
                    {
                        Eur = Convert.ToDecimal(node.Attributes["data-sweater-price-eur"].Value),
                        Gbp = Convert.ToDecimal(node.Attributes["data-sweater-price-gbp"].Value),
                        Usd = Convert.ToDecimal(node.Attributes["data-sweater-price-usd"].Value)
                    },
                    HoodiePrice = new ShirtPrice
                    {
                        Eur = Convert.ToDecimal(node.Attributes["data-hoodie-price-eur"].Value),
                        Gbp = Convert.ToDecimal(node.Attributes["data-hoodie-price-gbp"].Value),
                        Usd = Convert.ToDecimal(node.Attributes["data-hoodie-price-usd"].Value)
                    },
                    PrintPrice = new ShirtPrice
                    {
                        Eur = Convert.ToDecimal(node.Attributes["data-print-price-eur"].Value),
                        Gbp = Convert.ToDecimal(node.Attributes["data-print-price-gbp"].Value),
                        Usd = Convert.ToDecimal(node.Attributes["data-print-price-usd"].Value)
                    },
                    ImageLink = teeImage.ToList()[0].Attributes["src"].Value
                };
            
                _todaysTees.Add(tee);
            }
        }

        // GET api/shirts
        /// <summary>
        /// Retrieve today's and last chance shirts
        /// </summary>
        /// <returns>A enumerable object of ShirtModel</returns>
        [Route("api/shirts")]
        public IEnumerable<ShirtModel> GetTodayShirts()
        {
            return _todaysTees;
        }
        
        // GET api/shirts/{shirtId}
        /// <summary>
        /// Retrieve one tee from today's and last chance shirts
        /// </summary>
        /// <param name="shirtId">Id of the shirt (usually between 0 and 5)</param>
        /// <returns>A shirt model</returns>
        [Route("api/shirts/{shirtId}")]
        public ShirtModel GetShirtById(int shirtId)
        {
            return shirtId >= _todaysTees.Count ? null : _todaysTees[shirtId];
        }
    }
}
