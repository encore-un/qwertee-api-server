﻿using System.Net;
using HtmlAgilityPack;

namespace QwerteeApiServer.Models
{
    /// <summary>
    /// Static class used to retrieve qwertee html document and source
    /// </summary>
    public static class QwerteeSource
    {
        private static readonly WebClient Client = new WebClient();
        private static readonly HtmlDocument Document = new HtmlDocument();
        private static string _source;

        /// <summary>
        /// Get html source of qwertee website
        /// </summary>
        /// <returns>Html source as string</returns>
        public static string GetSource()
        {
            _source = Client.DownloadString("https://www.qwertee.com/");
            return _source;
        }

        /// <summary>
        /// Get html document of qwertee website
        /// </summary>
        /// <returns>Html document</returns>
        public static HtmlDocument GetDocument()
        {
            _source = Client.DownloadString("https://www.qwertee.com/");
            Document.LoadHtml(_source);
            return Document;
        }
    }
}