﻿
namespace QwerteeApiServer.Models
{
    /// <summary>
    /// Shirt model
    /// </summary>
    public class ShirtModel
    {
        /// <summary>
        /// Name of the shirt
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// URL of the shirt's image
        /// </summary>
        public string ImageLink { get; set; }
        /// <summary>
        /// Price for the tee format
        /// </summary>
        public ShirtPrice TeePrice { get; set; }
        /// <summary>
        /// Price for the hoodie format
        /// </summary>
        public ShirtPrice HoodiePrice { get; set; }
        /// <summary>
        /// Price of the sweater format
        /// </summary>
        public ShirtPrice SweaterPrice { get; set; }
        /// <summary>
        /// Price of the print format
        /// </summary>
        public ShirtPrice PrintPrice { get; set; }
    }
}