﻿
namespace QwerteeApiServer.Models
{
    /// <summary>
    /// Time left model
    /// </summary>
    public class TimeLeft
    {
        /// <summary>
        /// Hours left for today's sale
        /// </summary>
        public int Hours { get; set; }
        /// <summary>
        /// Minutes left for today's sale
        /// </summary>
        public int Minutes { get; set; }
        /// <summary>
        /// Seconds left for today's sale
        /// </summary>
        public int Seconds { get; set; }
    }
}