﻿
namespace QwerteeApiServer.Models
{
    /// <summary>
    /// Shirt price model
    /// </summary>
    public class ShirtPrice {

        /// <summary>
        /// Price in euros
        /// </summary>
        public decimal Eur { get; set; }
        /// <summary>
        /// Price in pounds sterling
        /// </summary>
        public decimal Gbp { get; set; }
        /// <summary>
        /// Price in US dollars
        /// </summary>
        public decimal Usd { get; set; }
    }
}